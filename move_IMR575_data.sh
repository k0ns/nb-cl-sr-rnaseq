cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198907-LR-30354/fastq/AS-198907-LR-30354_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO1.R1.fq.gz
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198907-LR-30354/fastq/AS-198907-LR-30354_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO1.R2.fq.gz

cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198908-LR-30354/fastq/AS-198908-LR-30354_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO2.R1.fq.gz
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198908-LR-30354/fastq/AS-198908-LR-30354_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO2.R2.fq.gz

cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198909-LR-30354/fastq/AS-198909-LR-30354_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO3.R1.fq.gz
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171107_D00759_0131_ACBPD9ANXX/AS-198909-LR-30354/fastq/AS-198909-LR-30354_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO3.R2.fq.gz

cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171122_SN7001427_0355_ACBLUTANXX/AS-200758-LR-30952/fastq/AS-200758-LR-30952_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO4.R1.fq.gz
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171122_SN7001427_0355_ACBLUTANXX/AS-200758-LR-30952/fastq/AS-200758-LR-30952_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO4.R2.fq.gz

cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171122_SN7001427_0355_ACBLUTANXX/AS-200759-LR-30952/fastq/AS-200759-LR-30952_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO5.R1.fq.gz
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171122_SN7001427_0355_ACBLUTANXX/AS-200759-LR-30952/fastq/AS-200759-LR-30952_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO5.R2.fq.gz

cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171122_SN7001427_0355_ACBLUTANXX/AS-200760-LR-30952/fastq/AS-200760-LR-30952_R1.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO6.R1.fq.gz
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_RNA/171122_SN7001427_0355_ACBLUTANXX/AS-200760-LR-30952/fastq/AS-200760-LR-30952_R2.fastq.gz /fast/users/helmsauk_c/scratch/nb-cl-rnaseq-fastq/Henssen_IMR575_DMSO6.R2.fq.gz
