import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-sr-rnaseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-sr-rnaseq/fastq/"
metadata = pd.read_csv(WORKING_DIR + "encode_metadata.csv", sep=";", comment="#")

# introduce a check that checks whehter metadata behaves well....

rule all:
  input:
    expand(FASTQ_DIR + "{e}.fq.gz", e=metadata.Sample.tolist())

rule get_data:
  output:
      FASTQ_DIR + "{e}.fq.gz"
  params:
      e = lambda wildcards: {wildcards.e},
      file_accession = lambda wildcards: metadata[metadata.Sample == wildcards.e].ExperimentAccession.values[0],
      fastq_link = lambda wildcards: metadata[metadata.Sample == wildcards.e].FastqFile.values[0],
      fq_dir = FASTQ_DIR
  shell:
      "rm -f {params.fq_dir}{params.file_accession}.fastq.gz && wget {params.fastq_link} -P {params.fq_dir} && mv {params.fq_dir}{params.file_accession}.fastq.gz {params.fq_dir}{params.e}.fq.gz"
