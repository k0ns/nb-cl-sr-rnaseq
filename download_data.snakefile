import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-sr-rnaseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-sr-rnaseq/fastq/"
metadata = pd.read_csv(WORKING_DIR + "metadata.csv", sep=";", comment="#")

rule all:
  input:
    expand(FASTQ_DIR + "{e}.fq.gz", e=metadata.Sample.tolist())

rule get_data:
  output:
      FASTQ_DIR + "{e}.fq.gz"
  params:
      e = lambda wildcards: {wildcards.e},
      srr = lambda wildcards: metadata[metadata.Sample == wildcards.e].SRR.values[0],
      ftp_prefix = lambda wildcards: metadata[metadata.Sample == wildcards.e].FastQ_FTP_Prefix.values[0],
      fq_dir = FASTQ_DIR
  run:
      shell("rm -f {params.fq_dir}{params.srr}.fastq.gz && wget {params.ftp_prefix}.fastq.gz -P {params.fq_dir} && mv {params.fq_dir}{params.srr}.fastq.gz {params.fq_dir}{params.e}.fq.gz")
